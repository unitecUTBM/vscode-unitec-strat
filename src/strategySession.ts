import * as vscode from 'vscode';
import * as request from 'request';
import * as fs from 'fs';

export class StrategySession {

    private workspacePath: vscode.Uri;
    private serverAddress: string;
    private timeout: number;

    constructor(workspacePath: vscode.Uri){
        this.workspacePath = workspacePath;

        const serverAddress = vscode.workspace.getConfiguration('').get('unitecStratServerAddress', "http://0.0.0.0:5000");
        this.serverAddress = serverAddress;

        const timeout = vscode.workspace.getConfiguration('').get('unitecTimeout', 200);
        this.timeout = timeout;
    }

    async setServer(){
		let result = await vscode.window.showInputBox({
			value: vscode.workspace.getConfiguration('').get('unitecStratServerAddress', 'http://0.0.0.0:5000'),
			prompt: "Unitec server input"
        });
        if (result){
            this.serverAddress = result;
        }
    }

    public getServer(): string{
        return this.serverAddress;
    }

    getSelectedFile(){
        request.get(this.serverAddress + "/scripts/selected", {timeout: this.timeout}, (error, response, body) => {
            if (error){
                vscode.window.showErrorMessage("Unitec Strat : error retrieving selected script : " + error.code);
                return;
            }
            if (body === ""){
                vscode.window.showInformationMessage("Unitec Strat : No selected file");
                return;
            }
            vscode.window.showInformationMessage("Unitec Strat : selected file is "+ body);
        });
    }

    setCurrentFileAsSelected(){
        const activeTextEditor = vscode.window.activeTextEditor;
        if (!activeTextEditor){
            vscode.window.showInformationMessage("Unitec Strat : no active file");
            return;
        }
        const filename = activeTextEditor.document.fileName.split('/').reverse()[0];
        request.post(
            this.serverAddress + "/scripts/" + filename + "/select",
            {timeout: this.timeout},
            (error, response, body) => {
                if (error) {
                    vscode.window.showErrorMessage("Unitec Strat : error selecting script : " + error.code);
                } else {
                    vscode.window.showInformationMessage("Unitec Strat : successfully selected " + filename);
                }
            }
        );
    }

    pull(): void{
        request.get(this.serverAddress + "/scripts", {timeout: this.timeout}, (error, response, body) => {
            if(error){
                vscode.window.showErrorMessage("Unitec Strat : failed to pull file : " + error.code);
                return;
            }
            for (let f of JSON.parse(body)){
                this.pullFile(f);
            }
        });
    }

    pullFile(name: string): void{
        request.get(this.serverAddress + "/scripts/" + name,{timeout: this.timeout}, (error, response, body) => {
            if (error){
                vscode.window.showErrorMessage("Unitec Strat : failed to pull file : " + error.code);
                return;
            }
            fs.writeFile(this.workspacePath.path + "/" + name, body, (err) => {
                if (err) {
                    vscode.window.showErrorMessage("Unitec Strat : error writing file");
                    return;
                }
                vscode.window.showInformationMessage("Unitec Strat : successfully pulled file")
            });                   
        });
        vscode.commands.executeCommand("workbench.action.files.revert");
    }

    pushCurrentFile(){
        const activeTextEditor = vscode.window.activeTextEditor;
        if (!activeTextEditor){
            vscode.window.showInformationMessage("Unitec Strat : no active file");
            return;
        }
        this.pushFile(activeTextEditor.document);
    }

    pushFile(document: vscode.TextDocument){
        if (document.languageId !== "python"){
            return;
        }
        request.put(
            this.serverAddress + "/scripts/" + document.fileName.split('/').reverse()[0],
            {
                body: document.getText(),
                timeout: this.timeout
            },
            (error, response, body) => {
                if (error) {
                    vscode.window.showErrorMessage("Unitec Strat : failed to push file : " + error.code);
                } else {
                    vscode.window.showInformationMessage("Unitec Strat : successfully pushed file");
                }
        });       
    }
}