import * as vscode from 'vscode';
import * as request from 'request';
import {StrategySession} from './strategySession';

export function activate(context: vscode.ExtensionContext) {
	const workspaceFolders = vscode.workspace.workspaceFolders;
	if (!workspaceFolders){
		deactivate();
		return;
	}
	let strategySession = new StrategySession(workspaceFolders[0].uri);
	vscode.commands.registerCommand('extension.activate', () => {});
	vscode.commands.registerCommand('extension.pull', () => { strategySession.pull(); });
	vscode.commands.registerCommand('extension.pushCurrentFile', () => { strategySession.pushCurrentFile(); });
	vscode.commands.registerCommand("extension.getSelectedFile", () => strategySession.getSelectedFile());
	vscode.commands.registerCommand("extension.setServer", () => strategySession.setServer());
	vscode.commands.registerCommand("extension.setCurrentFileAsSelected", () => strategySession.setCurrentFileAsSelected());

	vscode.workspace.onDidSaveTextDocument((document: vscode.TextDocument) => {
		const isPushOnSave = vscode.workspace.getConfiguration('').get('unitecPushOnSave', "");
		if (isPushOnSave){
			strategySession.pushFile(document);
		}
	});

	vscode.window.showInformationMessage('Unitec Strat : extension activated, current server : ' + strategySession.getServer());
}

export function deactivate() {
	vscode.window.showInformationMessage("Unitec Strat extension deactivated");
}
